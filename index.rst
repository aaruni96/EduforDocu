.. EduFor documentation master file, created by
   sphinx-quickstart on Sat Dec 19 10:36:05 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to EduFor's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   onboarding/install
   onboarding/quickstart-android
   onboarding/quickstart-iOS
   onboarding/quickstart-windows
   onboarding/quickstart-linux
   onboarding/quickstart-macOS
   onboarding/e2ee

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

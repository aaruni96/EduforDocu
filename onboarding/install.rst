========================
EduFor Onboarding Manual
========================

**Welcome to EduFor: your privacy respecting cloud provider.**

EduFor provides an all-in-one privacy focused cloud suite. Built with the greatest Free and Open Source tools, we provide industry grade infrastructure to the everyday Joe.

Our email service is built upon the rock solid foundations of Postfix and Dovecot. We strictly adhere to SMTP and IMAP protocols, so you can use your favorite client for email. All of our email is signed and TLS encrypted with the strongest keys. We are rated an A by SSL Labs' security evaluation. We are also whitelisted by dnswl.org , and almost never end up in spam.

EduFor private cloud is powered by Nextcloud, the best in class FLOSS cloud solution. All your data can be synced across all your devices via the universal WebDAV protocol. You can also sync your contacts and calendar events via CardDAV and CalDAV respectively. Our service is compliant with all hardening and security recommendations, and is rated A+ by the Nextcloud security scanner. 
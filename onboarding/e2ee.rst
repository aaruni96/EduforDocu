==========
Encryption
==========

Default Protections
-------------------

All communication to and from the server are TLS encrypted. We comply with all hardening and security recommendations applicable to our service, and are rated well by all popular security scanners.

.. figure:: images/sslscan.jpeg

   SSL Secirty Scan Rating

All data is also doubly encrypted at rest, with randomised keys generated based on your initial password.

Email E2EE
----------

Optionally, you can set up end to end encrypted email using PGP or S/MIME schemes.

We recommend the S/MIME scheme as `PGP has its own set of problems <https://latacora.micro.blog/2019/07/16/the-pgp-problem.html>`_ .

S/MIME
######

For S/MIME based encryption, you need to have a PKS12 private certificate. ACTALIS is a good place to get free S/MIME certificates with one year validity and will be detailed in this guide, but you can use any valid certificate for end-to-end encryption of your email.



Get certificate
***************
- Fill your email into  `this form <https://extrassl.actalis.it/portal/uapub/freemail?lang=en>`_ . Fill the captcha, and hit ``Send verification email``. You will get an email with a verification code, which you then have to enter into the same form.
   .. image:: images/e2ee01.png
   
- Fill the verification code from the email into the form, hit captcha again, tick the required declarations, and click ``Submit Request``. Note that you don't have to consent to the marketing subscriptions.
   .. image:: images/e2ee02.png

- You will get your certificate in your email, and the password will be displayed in the browser window. **DO NOT LOSE THIS PASSWORD**.
   .. image:: images/e2ee03.png

- Download the certificate from your email and unzip it somewhere on your device. Remember, you will need the password displayed in the previous step to actually use this certificate.

PC
***

Import into Thunderbird
.......................

- Open the main account screen in Thunderbird. Select ``End-to-end Encryption``
   .. image:: images/e2ee04.png

- In the settings page which appers, select ``Manage S/MIME Certificates``.
   .. image:: images/e2ee05.png

- Under ``Your Certificates``, click ``Import``.
   .. image:: images/e2ee06.png

- Choose the certificate you have just unzipped. Enter the password which was displayed to you when you were issued the certificate. Your certificate will then be added to thunderbird.

- Next, setup thunderbird to actually use the certificate, by choosing this as the personal certificate for digital signing and encryption.
   .. image:: images/e2ee07.png

Setup Defaults
..............

- If you want, you can enable signing and encryption by default. Note that you will only be able to send encrypted emails to people whose public certificate you already have. This is done by recieving an S/MIME signed email from them.

- On the End-to-end encryption setting page for your account, scroll down to the default encryption settings. The recommended settings are to sign emails by default, and not encrypt emails by default.
   .. image:: images/e2ee08.png

Android
********

In android, certificates are handled centrally, not on a per application basis.

- Open the settings app, and navigate to the Security page.

- Open the Encryption and credentials tab.

- Select ``Install from SD card``

- Navigate to where you have saved your certificate file. It should have a ``.pfx`` extension.

- When prompted, input your password. This certificate will now be imported and available as an encryption method in all apps.

   Note that the FairEmail app supports encrypting emails as a pro feature, but will be able to decrypt emails using your certificate for free.
==========================
Quickstart Guide - Android
==========================

This guide will quickly get you through the setup phase of android apps and integrations with the EduFor suite.

This guide will make use of the `F-Droid <https://f-droid.org/>`_ app store on Android, and the apps `FairEmail <https://f-droid.org/en/packages/eu.faircode.email/>`_, `Nextcloud Syncronisation Client <https://f-droid.org/en/packages/com.nextcloud.client/>`_, and `DAVx^5 <https://f-droid.org/en/packages/at.bitfire.davdroid/>`_ from the F-Droid store.

Your experience using other apps and sources for these apps might differ.

Email
-----


* Open FairEmail. You will see the setup wizard when you open it for the first time. Choose "Other provider".
    .. image:: images/email01.jpg
* Enter your name, your email address, and your password in the resulting screen.
    .. image:: images/email02.jpg
* EduFor supports email autoconfiguration for Thunderbird compatible clients, so the server details will be auto filled, and will look like this
    .. image:: images/email03.jpg
* You will see a confirmation screen
    .. image:: images/email04.jpg
* You are now ready to use your email account!
    .. image:: images/email05.jpg

Nextcloud Sync Client
---------------------
* Open the Nextcloud app. You will see the welcome screen. Select the choice to Login.
    .. image:: images/nc01.jpg
* Enter the server address in the next screen. It is ``https://cloud.edufor.me`` . The ``s`` in ``https`` is important.
    .. image:: images/nc02.jpg
* Click `Log In` on the next screen. You will be redirected to a browser window where you will have to enter your username and password. (Remember, your nextcloud password may be different from your email password!)
    .. image:: images/nc03.jpg
* You will then be able to see and manage your files in the Nextcloud app!
    .. image:: images/nc04.jpg

CardDAV
-------
* Open the nextcloud app, hit the burger menu, and navigate to settings.
    .. image:: images/dav01.jpg
* Select the option to syncronise contacts and calendars. (Note: Avoid selecting the option to backup up contacts. We won't need it after setting up DAVx5.)
    .. image:: images/dav02.jpg
* On the resulting add account screen, select ``Groups as per-contact categories``. Then hit create account.
    .. image:: images/dav03.jpg
* On the resulting screen, check Contacts, then hit the sync button.
    .. image:: images/dav04.jpg

CalDAV
------

    This section is only valid once you have a calendar setup in your account.